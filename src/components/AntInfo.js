/**
 * @module AntInfo
 * @summary a componente for display ant info
 *  */
import React from 'react';
import {Text, View, StyleSheet, ActivityIndicator} from 'react-native';
import {IS_CALCULATING} from '../redux/AntsSilce';

const AntInfo = item => {
  const {name, length, color, weight, possibility} = item.item;
  return (
    <View style={styles.card}>
      <Text style={styles.title}>{name}</Text>
      <View style={styles.statusContainer}>
        <Text>length: {length}</Text>
        {possibility === IS_CALCULATING && (
          <View style={styles.status}>
            <ActivityIndicator size="small" color="#0000ff" />
          </View>
        )}
      </View>
      <Text>color: {color}</Text>
      <Text>weight: {weight}</Text>
      {possibility && (
        <Text>
          possibility:{' '}
          {possibility === IS_CALCULATING ? 'calculating...' : possibility}
        </Text>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    borderColor: 'blue',
    borderStyle: 'solid',
  },
  title: {
    fontWeight: 'bold',
  },
  card: {
    backgroundColor: '#F6FAFE',
    padding: 25,
  },
  statusContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  status: {
    alignItems: 'center',
  },
});

export default AntInfo;
