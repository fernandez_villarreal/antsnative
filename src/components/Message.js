/**
 * @module Message
 * @summary small component to display a default message
 */
import React from 'react';

import {View, Text, StyleSheet} from 'react-native';

const Message = ({message}) => {
  return (
    <View style={styles.container}>
      <Text style={styles.message}>{message}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
  message: {
    textAlign: 'center',
    fontSize: 18,
    color: 'gray',
    marginBottom: 20,
  },
});

export default Message;
