import React from 'react';

import {View, StyleSheet} from 'react-native';

const ItemSeparator = () => {
  return <View style={styles.cotainer} />;
};

const styles = StyleSheet.create({
  cotainer: {
    height: 1,
    width: '100%',
    backgroundColor: '#CED0CE',
  },
});

export default ItemSeparator;
