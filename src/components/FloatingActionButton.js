/**
 * @module FloatingActionButton
 * @summary FloatingActionButton component. handle press event passed in as prop.
 */
import React from 'react';

import {Pressable, StyleSheet, Text, View} from 'react-native';

// Const for style when pressed or disabled
const STYLE_DISABLE = 'silver';
const STYLE_ENABLE = '#2196F3';

const FloatingActionButton = ({title, handlePress, disable}) => {
  return (
    <Pressable
      style={({pressed}) => [
        {
          backgroundColor: pressed
            ? STYLE_DISABLE
            : disable
            ? STYLE_DISABLE
            : STYLE_ENABLE,
        },
        styles.container,
      ]}
      disabled={disable}
      onPress={handlePress}>
      <View style={styles.textIconContainer}>
        <Text style={styles.textIcon}>{title}</Text>
      </View>
    </Pressable>
  );
};

const styles = StyleSheet.create({
  container: {
    width: 60,
    height: 60,
    borderRadius: 30,
    position: 'absolute',
    bottom: 10,
    right: 10,
    flex: 1,
  },
  textIconContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 0,
  },
  textIcon: {
    fontSize: 30,
    color: 'white',
  },
});

export default FloatingActionButton;
