import {createSlice} from '@reduxjs/toolkit';
import {createAsyncThunk} from '@reduxjs/toolkit';
import {fetchAnts} from '../Api/services';
import getWinOdds from '../helpers/getWinOdds';

const initialState = {
  items: [],
  isLoading: false,
  loadingError: null,
};

// value for ant odds that is being calculated (smaller than 0 for sorting reasons)
export const IS_CALCULATING = -1;

export const getAnts = createAsyncThunk(
  'ants/getAnts',
  async (data, thunkApi) => {
    try {
      const response = await fetchAnts();
      return response;
    } catch (err) {
      // error handling was not specified in the challenge so i do something basic
      return thunkApi.rejectWithValue({
        error: 'Houston we have a problem! Try again?',
      });
    }
  },
);

export const calculateAntPossibility = createAsyncThunk(
  'ants/calculateAntPossibility',
  async (antsName, thunkApi) => {
    const possibility = await getWinOdds();
    return {
      possibility: possibility.toFixed(2),
      name: antsName,
    };
  },
);

const antsSlice = createSlice({
  name: 'ants',
  initialState,
  reducers: {
    resetPossibility: state => {
      state.items.forEach(ant => {
        ant.possibility = 'not yet calculated';
      });
    },
  },
  extraReducers: {
    [getAnts.pending]: state => {
      state.isLoading = true;
    },
    [getAnts.fulfilled]: (state, action) => {
      state.items = action.payload.ants;
      state.isLoading = false;
    },
    [getAnts.rejected]: (state, action) => {
      state.loadingError = action.payload.error;
      state.isLoading = false;
    },
    [calculateAntPossibility.pending]: (state, {meta: {arg: name}}) => {
      const ant = state.items.find(item => item.name === name);
      ant.possibility = IS_CALCULATING;
    },
    [calculateAntPossibility.fulfilled]: (
      state,
      {payload: {possibility, name}},
    ) => {
      // first chage the state of the ant
      const ant = state.items.find(item => item.name === name);
      ant.possibility = possibility;

      // then sort ants by possibility
      state.items.sort((a, b) => {
        return b.possibility - a.possibility;
      });
    },
  },
});

export const {resetPossibility} = antsSlice.actions;

export const antsReducer = antsSlice.reducer;
