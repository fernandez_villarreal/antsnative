import {configureStore} from '@reduxjs/toolkit';
import {antsReducer} from './AntsSilce';

export const store = configureStore({
  reducer: {
    ants: antsReducer,
  },
});
