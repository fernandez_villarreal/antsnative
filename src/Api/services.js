const MAIN_URL = 'https://sg-ants-server.herokuapp.com';

export const fetchAnts = async () => {
  const response = await fetch(`${MAIN_URL}/ants`);
  return response.json();
};
