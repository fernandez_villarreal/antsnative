/**
 * @module AntsScreen
 * @summary Screen for fetching ants and showing them
 */
import React from 'react';
import {useSelector, useDispatch, batch} from 'react-redux';
import {
  FlatList,
  StyleSheet,
  ActivityIndicator,
  View,
  Button,
  Text,
} from 'react-native';

import AntInfo from '../components/AntInfo';
import FloatingActionButton from '../components/FloatingActionButton';
import Message from '../components/Message';
import ItemSeparator from '../components/ItemSeparator';

import {
  getAnts,
  calculateAntPossibility,
  resetPossibility,
  IS_CALCULATING,
} from '../redux/AntsSilce';

const AntsScreen = () => {
  const {
    items: ants,
    isLoading,
    loadingError,
  } = useSelector(state => state.ants);
  const dispatch = useDispatch();

  const handleGetAntsPress = () => {
    dispatch(getAnts());
  };

  const handlePressRace = () => {
    batch(() => {
      dispatch(resetPossibility());
      for (let item of ants) {
        dispatch(calculateAntPossibility(item.name));
      }
    });
  };

  const hasPossibility = ants.some(ant => ant.possibility);
  const isRacing = ants.some(ant => ant.possibility === IS_CALCULATING);

  return (
    <View style={styles.container}>
      <FlatList
        ListHeaderComponent={
          ants.length > 0 &&
          hasPossibility && (
            <StatusRace
              ants={ants}
              isLoading={isLoading}
              hasPossibility={hasPossibility}
              isRacing={isRacing}
            />
          )
        }
        contentContainerStyle={styles.flatContainer}
        data={ants}
        renderItem={({item}) => <AntInfo item={item} />}
        keyExtractor={item => item.name}
        ListEmptyComponent={
          isLoading ? (
            <ActivityIndicator
              style={styles.spinner}
              size="large"
              color="#0000ff"
            />
          ) : (
            <LoadingButton
              loadingError={loadingError}
              handleGetAntsPress={handleGetAntsPress}
            />
          )
        }
        ItemSeparatorComponent={ItemSeparator}
      />
      {ants.length > 0 && (
        <FloatingActionButton
          title="GO"
          handlePress={handlePressRace}
          disable={isRacing}
        />
      )}
    </View>
  );
};

const LoadingButton = ({loadingError, handleGetAntsPress}) => {
  return (
    <View style={styles.cntEmpty}>
      {loadingError && <Message message={loadingError} />}
      <Button
        style={styles.loadBtn}
        title={'Load Ants'}
        onPress={handleGetAntsPress}
      />
    </View>
  );
};

const StatusRace = ({ants, isLoading, hasPossibility, isRacing}) => {
  const getStatus = () => {
    if (isRacing) {
      return 'Racing';
    }

    if (!isRacing && !isLoading && hasPossibility) {
      return 'Race finished';
    }

    if (!isRacing && isLoading) {
      return null;
    }
  };

  return (
    <View style={styles.statusHeaderContainer}>
      <Text style={styles.statusHeaderText}>{getStatus()}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: '100%',
  },
  flatContainer: {
    flexGrow: 1,
    marginTop: 0,
    paddingTop: 0,
  },
  cntEmpty: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  loadBtn: {
    backgroundColor: 'silver',
    width: 90,
    height: 30,
    margin: 5,
    borderRadius: 5,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textButton: {
    marginTop: 2,
    textAlign: 'center',
    color: '#fff',
    fontSize: 18,
  },
  spinner: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  statusHeaderContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 0,
    paddingBottom: 0,
    borderBottomColor: '#CED0CE',
    borderBottomWidth: 2,
  },
  statusHeaderText: {
    fontSize: 18,
    margin: 5,
  },
});

export default AntsScreen;
