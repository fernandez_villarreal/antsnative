function generateAntWinLikelihoodCalculator() {
  const delay = 7000 + Math.random() * 7000;
  const likelihoodOfAntWinning = Math.random();

  return callback => {
    setTimeout(() => {
      callback(likelihoodOfAntWinning);
    }, delay);
  };
}

/**
 * @method getWinOdds
 * @description a helper function to calculate the likelihood of a win in a promise
 * @returns number
 * */
const getWinOdds = () => {
  return new Promise(resolve => {
    return generateAntWinLikelihoodCalculator()(resolve);
  });
};

export default getWinOdds;
