## AntsNative - Challenge

A simple app to get ants and run with them

## Requirements info

[link](https://stadiumgoodshq.notion.site/Mobile-App-Hiring-Challenge-82bc55b799f84b19af6960eb96b51309)

## Installation for node_packages

Use your favorite package manager to install dependencies.

## npm

```bash
npm install
```

## yarn

```bash
yarn install
```

## Installation for Cocoapods

```pod
pod install
```

## Libraries used

- [React Native](https://reactnative.dev)
- [Redux Toolkit](https://redux-toolkit.js.org)

## Usage npm

```bash
npm start

# in another terminal for android
npm run android

# in another terminal for iOS
npm run iOS
```

## Usage yarn

```yarn
yarn run start

# in another terminal for android
yarn run android

# in another terminal for iOS
yarn run iOS
```

## License

[MIT](https://choosealicense.com/licenses/mit/)
