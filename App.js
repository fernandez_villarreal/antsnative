/**
 * @module App
 * @summary main component of app
 */

import React from 'react';
import {StatusBar, SafeAreaView} from 'react-native';
import AntsScreen from './src/screens/AntsScreen';

import {Provider} from 'react-redux';
import {store} from './src/redux/store';

/**
 * @method App
 * @description App component that renders the main screen of the app and set navigation and state with redux
 */
const App = () => {
  return (
    <Provider store={store}>
      <StatusBar />
      <SafeAreaView>
        <AntsScreen />
      </SafeAreaView>
    </Provider>
  );
};

export default App;
